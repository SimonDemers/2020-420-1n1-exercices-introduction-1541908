package ca.cegepdrummond;
import java.util.Scanner;

public class Serie4_EntreesClavier {
    public void clavier1() {
        Scanner clavier1 = new Scanner(System.in);
        int num1;
        num1 = clavier1.nextInt();
        System.out.println(num1);
    }

    /*
     * Corrigez ce code afin qu'il affiche le nombre qui sera fourni en entré.
     */
    public void clavier2() {
        Scanner clavier2 = new Scanner(System.in);
        int num1 = clavier2.nextInt();
        System.out.println(num1);
     }

    /*
     * Modifiez ce code afin qu'il demande une chaine de caractère suivit d'un entier
     * (ils peuvent être sur la même ligne ou sur deux lignes différentes).
     * Vous devez ensuite afficher la chaine de caractère sur la ligne et l'entier sur la ligne suivante.
     */
    public void clavier3() {

         Scanner clavier = new Scanner(System.in);
         String chaine1 = clavier.nextLine();
         int num1 = clavier.nextInt();
         System.out.println(chaine1);
         System.out.println(num1);


    }

    /*
     * Programmer une fonction qui demande 3 chaines de caractères et les affiche ensuite dans l'ordre inverse.
     * Exemple:
     * monde
     * le
     * allo
     *
     * affichera:
     * allo
     * le
     * monde
     *
     */
    public void clavier4() {
        System.out.println("Entrez 3 mots");
        Scanner clavier1 = new Scanner(System.in);
        String mot1 = clavier1.next();
        String mot2 = clavier1.next();
        String mot3 = clavier1.next();
        System.out.println(mot3);
        System.out.println(mot2);
        System.out.println(mot1);
    }

    /*
     * Modifiez ce code pour qu'il demande 5 mots sur une même ligne et les affiches sur des lignes successives.
     *
     * Exemple:
     * Les cinq mots à lire
     *
     * affichera:
     * Les
     * cinq
     * mots
     * à
     * lire
     *
     * indice: next vs nextLine.
     */
    public void clavier5() {
        Scanner clavier = new Scanner(System.in);
        /* enlever cette ligne de commentaire
        String mot1 = clavier.????
        String mot2 =
        .
        .
        System.out.println(mot5);
        enlever cette ligne de commentaire */
    }

    /*
     * Modifiez ce code afin qu'il inverse les deux valeurs entrées.
     *
     * exemple:
     * un
     * deux
     *
     * affichera
     * deux un
     *
     *
     * Note: cette technique pour inverser 2 valeurs en utilisant une valeur intermédiaire
     * est souvent utilisée.
     * Si vous ne comprenez pas le fonctionnement, veuillez demander au professeur.
     *
     */
    public void clavier6() {
        Scanner s = new Scanner(System.in);
        String valeur1 = s.next();
        String valeur2 = s.next();
        String intermediaire;
        intermediaire = valeur1;
        /* enlever cette ligne de commentaire
        valeur1 = ???
        valeur2 = ???
        enlever cette ligne de commentaire */
        System.out.println(valeur1 + " " + valeur2); // <<<< ne modifiez pas cette ligne

    }



}
