package ca.cegepdrummond;

import java.util.Scanner;

public class Serie6_String {
    /*
     * Modifiez ce code pour afficher la longueur de la chaine de caractères entrée.
     */
    public void string1() {
        Scanner s = new Scanner(System.in);
        String i = s.nextLine();
        int len = i.length();
        System.out.println("La longueur est: " + len);

    }

    /*
     * Modifiez le code afin de valider si les 2 chaines entrées sont pareils.
     * Si elles sont égales, affichez "pareils".
     * Si non, affichez "différentes"
     *
     * Exemple:
     * allo
     * allo
     * affichera: pareils
     *
     * Exemple 2:
     * allo
     * allo1
     * affichera: différentes
     */
    public void string2() {
        Scanner s = new Scanner(System.in);
        String s1 = s.nextLine();
        String s2 = s.nextLine();

        if (s1.equals(s2)) {
            System.out.println("pareils");
        } else {
            System.out.println("différentes");
        }

    }

    /*
     * Modifiez le code afin de valider si les 2 chaines entrées sont pareils sans considérer les majuscules et minuscules.
     * Si elles sont égales, affichez "pareils".
     * Si non, affichez "différentes"
     *
     * Exemple:
     * allo
     * allo
     * affichera: pareils
     *
     * Exemple 2:
     * allo
     * ALLo
     * affichera: pareils
     */
    public void string3() {
        Scanner s = new Scanner(System.in);
        String s1 = s.nextLine();
        String s2 = s.nextLine();

        if (s1.equalsIgnoreCase(s2)) {
            System.out.println("pareils");
        } else {
            System.out.println("différentes");
        }

    }

    /*
     * Remplacez les ?? afin que le code affiche entre guillemets (" ") ce qui a été entré.
     * Vous devez donc afficher des " "
     *
     * indice: caractère d'échappement ( \ )
     */
    public void string4() {
        Scanner s = new Scanner(System.in);
        String s1 = s.nextLine();

        System.out.println("La chaine est \"s1\"");


    }

    /*
     * Modifiez le code afin de demander 2 chaines de caractères.
     * Vous devez ensuite afficher ces 2 chaines entre guillemet et mettre le caractère \ entres les deux.
     *
     * Exemples:
     * Allo
     * Hello
     * Affichera:
     * "Allo"\"Hello"
     */
    public void string5() {
        Scanner s = new Scanner(System.in);
        String s1 = s.nextLine();
        String s2 = s.nextLine();

        System.out.print(s1);
        System.out.print("\\");
        System.out.println(s2);

    }

    /*
     * Ce code demande une chaine de caractères ainsi qu'un entier.
     * Modifiez le afin qu'il affiche le caractère se trouvant à la position désignée par l'entier.
     *
     * N'oubliez pas que le premier caractère est à la position 0
     * et que le dernier est à la position longueur-1.
     *
     * Vous devez vérifier si l'entier en entrée est valide ( >=0 ET < longueur).
     * S'il n'est pas valide, afficher "invalide".
     *
     * Exemple:
     * allo
     * 3
     * Affichera:
     * o
     *
     * Exemple2:
     * bonjour
     * 7
     * Affichera:
     * invalide
     */
    public void string6() {
        Scanner s = new Scanner(System.in);
        String chaine = s.nextLine();
        int position = s.nextInt();
        int longueurF = chaine.length();
        int longueur = longueurF - 1;
        char w = chaine.charAt(longueur);

        if (position >= 0 && 0 < longueur)
            System.out.println(w);  //<<<< affichez le caractère à la position demandée
    }else

    {
        System.out.println("invalide");

    }

}

}
