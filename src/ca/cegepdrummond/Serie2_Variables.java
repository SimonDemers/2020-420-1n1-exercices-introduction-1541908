package ca.cegepdrummond;

public class Serie2_Variables {
    /*
     * Complétez le code suivant afin de déclarer la
     * variable "varA" comme étant du type int
     * et ayant la valeur 1234
     */
    public void variable1() {
        int varA = 1234;
        System.out.println(varA);
    }

    /*
     * Assignez des valeurs aux variables a,b,c
     * a doit avoir la valeur 3
     * b doit avoir la valeur 4
     * c doit avoir la valeur 5
     *
     * Le println doit afficher  3 4 5
     */
    public void variable2() {
        int a = 3;
        int b = 4;
        int c = 5;

        System.out.println(a + " " + b + " " + c );
    }

    /*
     * Ajoutez le nombre minimal de parenthèses () pour que ce code affiche 42
     */
    public void variable3() {
        int a = 42 + (3 + 4 - 7 * 1) ; // ajoutez des parenthèses afin de regrouper les opérateurs mathématiques

        System.out.println( a );
    }

    /*
     * corrigez le code pour qu'il affiche le caractère 'a'
     */
    public void variable4() {
        char x = 'a';
        System.out.println(x);
    }



}
