package ca.cegepdrummond;

/**
 * Cette classe contient des exemples de certaines notions de base.
 * Vous pouvez vous y référez pour mieux comprendre la théorie.
 *
 */
public class Exemples {
    /*
     * Exemples de sortie à l'écran (print)
     */
    public void exemplePrint1() {
        System.out.println(); // affiche une ligne vide
        System.out.println(""); // autre facon d'afficher une ligne vide
        System.out.print("allo"); // affiche allo mais reste sur la même ligne
        System.out.println(" toi"); // affiche toi au bout de allo

        System.out.println(1); // affiche un entier












    }

    /**
     * exemples de commentaire  <<<< et ceci est un commentaire de documentation Java
     * @param x: un entier qui sert a faire blahblahblah       <<<< documentation des paramètres de la fonction
     * @return  expliquer ce qui est retourné    <<<< documentation de la valeur de retour de la fonction
     *
     *    Il est important d'indiquer ce que "fait" ou "à quoi sert" un paramètre. Si vous avez un paramètre
     *    appelé "age", il est un peu inutile de mettre "age" comme commentaire. Ca ajoute rien.
     *    Si les paramètres sont bien nommés, le commentaire peut être facultatif.
     *    exemple: si le paramètre est nommé "age" et que vous devez ajouter un commentaire pour indiquer
     *    que c'est l'age au moment de l'adoption, pourquoi ne pas simplement appeler la variable ageAdoption.
     *
     *    Pour la valeur de retour, ici le nom de la fonction devrait permettre de déduire assez facilement ce
     *    qu'elle retourne.
     *    exemple: si la fonction s'appel faitCalcul() et que vous devez indiquer que le return est la
     *    valeur x*42, pourquoi ne pas avoir appelé la fonction multiplierPar42().
     *
     */
    public int exempleCommentaire(int x) {
        /* début d'un commentaire multi lignes
              <<<< le commentaire multi lignes sert souvent pour empêcher l'exécution d'un bout de code afin de
              <<<< faire des tests (débugger)
        System.out.println(1);   <<<< ce code n'est pas exécuté
        fin d'un commentaire multiligne */

        //System.out.println();   <<<< commentaire sur une ligne complète .
        System.out.println(); // commentaire de fin de ligne. Sert à documenter ce que fait cette ligne.
        System.out.println(); // il est recommandé de ne pas écrire des lignes trop longue lorsqu'on écrit du code. Si vous écrivez de long commentaires, ils deviennent difficiles à lire car il faut faire défiller l'écran pour les lire.
                              // il est donc préférable d'écrire un long commentaire sur plusieurs lignes.
        return 0;
    }

    /*
     * exemple de variables.
     */
    public void exempleVariable() {
        int a;  // déclaration de la variable a.
        int b = 0; // déclaration et assignation de la variable b
        float uneValeurEnVirguleFlottante; // notez le début avec une minuscule, et une majuscule à chaque mot.

        final int laVieLUniversEtLeReste = 42; // une constante. Cette valeur ne peut être changée à cause de "final".

        String A; // notez que la variable "a" et la variable "A" sont deux variables. Java est sensible à la case.

        int unEntier = 1;
        char unCaractere = 'a'; // notez le guillement simple ' dans le cas d'un caractère.
        String unChaineDeCaracteres = "une phrase"; // notez le S majuscule à String, et les guillemets double "
                // notez aussi que je n'ai pas écris Caratères (avec l'accent). Bien qu'il soit valide d'utiliser
                // des accents pour le nom de variables, il est recommandé de ne pas le faire. Vous verrez plus tard
                // que la gestion des accents peut causer quelques problèmes en général.
                // Le fait de ne pas utiliser d'accent peut faire en sorte que le nom la variable perde sont sens.
                // Par exemple: si une variable doit indiquer qu'un item a été "livré", et que vous appeler la variable
                // estLivre, on se demandera pourquoi vous voullez savoir si c'est un livre. On fini par comprendre,
                // mais vous verrez que ce genre de "mauvais sens" (on dit sémantique) fait que le code peut devenir
                // difficile à lire. Essayez de trouver des noms sans accents (des fois c'est difficile).
        float unFloat = 1.0f; // notez que pour assigner une valeur à un float, il faut mettre un "f" à la fin de la valeur
        double unDouble = 1.0;
    }

    /*
     * exemple d'opérations mathématiques de base
     */
    public void exempleOperationsMathematiquesBase() {
        int a = 2;  // notez que la variable a dans cette fonction n'est pas la même que dans la fonction précédente.
        int b = 20;
        int c = 3;
        float d = 3f;

        System.out.println(a + b); // affiche 22
        System.out.println(b - a); // affiche 18
        System.out.println(a * b); // affiche 40
        System.out.println(b / a); // affiche 10
        System.out.println(b / c); // affiche 6  (c'est la division entière)
        System.out.println(b / d); // affiche 6.6666665  (c'est la division en virgule flottante car d est un float)
        //System.out.println(b / (c-f)); // afficherait une erreur de division par 0
        System.out.println(b % c); // affiche 2  (3 entre 6 fois dans 20, et il reste 2)


    }
}

