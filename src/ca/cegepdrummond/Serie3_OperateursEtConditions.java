package ca.cegepdrummond;

public class Serie3_OperateursEtConditions {
    /*
     * Remplacez les ??? dans le code afin que "choix 1" soit affiché.
     */
    public void operateur1() {
        int a = 3;
        int b = 6;

        if (b > a) {
            System.out.println("choix 1");
        } else {
            System.out.println("choix 2");
        }
        ;
    }

    /*
     * Remplacez les ?? pour que "Réponse A" soit affichée
     */
    public void operateur2() {
        int a = 3;
        int b = 6;
        int c = 2;

        if (b % a == c - b / a) {
            System.out.println("Réponse A");
        } else {
            System.out.println("Réponse B");
        }
        ;
    }

    /*
     * Remplacez les ?? pour que "Réponse B" soit affiché
     *
     * indice: operateur de négation.
     */
    public void operateur3() {
        int a = 3;
        int b = 6;
        int c = 2;

        if ( !(b % a == c - b / a) ) {
               System.out.println("Réponse A");
        } else {
            System.out.println("Réponse B");
        };
    }

    /*
     * Dans l'exercise précédent, nous vous avions demandé d'utiliser une négation
     * afin d'obtenir Réponse B.
     * Cette fois-ci, changez les ?? pour un autre opérateur afin d'obtenir Réponse B.
     *
     * indice: pas égal.
     */
    public void operateur4() {
        int a = 3;
        int b = 6;
        int c = 2;

        if ( b % a != c - b / a )  {
               System.out.println("Réponse A");
        } else {
            System.out.println("Réponse B");
        };
    }

    /*
     * Remplacer les premiers ?? pour inscrire la formule suivante: (a multiplié par b) modulo c
     * et remplacez les deuxièmes ?? par la valeur qui fera en sorte que "Réponse A" soit affiché.
     */
    public void operateur5() {
        int a = 3;
        int b = 6;
        int c = 2;

        if ( (a*b)%c == 0 ) {
            System.out.println("Réponse A");
        } else {
            System.out.println("Réponse B");
        }
    }

    /*
     * remplacer les ?? afin de créer une fonction qui fait les opérations suivantes:
     *   -créer 3 variables de type int nommées a, b, c
     *   -mettre la valeur 4 dans a
     *   -mettre la valeur 5 dans b
     *   -mettre la somme de a et b dans c
     *   -Afficher le texte suivant:  "La somme est "  suivit de la valeur de c
     *      Ca devrait donc afficher "La somme est 9"
     */
    public void operateur6() {
        int a = 4;
        int b = 5;
        int c = 9;
        String s = "La somme est ";
        if (a + b == c)
            System.out.print(s);
            System.out.print(c);

    }

    /*
     * Complétez le code pour qu'il affiche "bravo"
     */
    public void condition1() {
        int a = 3;
        int b = 5;
        if ( b > a ) {
            System.out.println("bravo");
        }
    }

    /*
     * Corrigez le code afin d'afficher "choix 2"
     */
    public void condition2() {
        int a = 3;
        int b = 3;
        if ( a < b ) {
            System.out.println("choix 1");
        } else {
            System.out.println("choix 2");
        }
    }

    /*
     * Complétez le code pour que "choix 3" soit affichée
     */
    public void condition3() {
        int a = 5;
        int b = 4;
        if (a < b) {
            System.out.println("choix 1");
        } else if (a == b) {
            System.out.println("choix 2");
        } else  {
            System.out.println("choix 3");
        }
    }


}
