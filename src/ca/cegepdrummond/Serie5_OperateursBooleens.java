package ca.cegepdrummond;

import java.util.Scanner;

public class Serie5_OperateursBooleens {

    /*
     * NOTE: pour les numéros de cette série, il peut y avoir plusieurs réponses valides.
     * Évaluez la réponse avant de l'essayer.
     *
     * Essayez-en plusieurs pour valider que vous comprenez bien la logique booléenne.
     * Bien comprendre cette logique est primordiale en informatique.
     * Si vous avez de la difficulté avec ces opérations, n'hésitez pas à demander
     * des explications auprès du professeur ou de vos ami(e)s.
     *
     */


    /*
     * Changez les valeurs de valeur1 et valeur2 afin que "bravo" soit affiché
     *
     * Note: il est possible que les valeurs originales soient bonnes!
     */
    public void bool1() {
        boolean valeur1, valeur2;

        valeur1 = true;
        valeur2 = true;

        if (valeur1 && valeur2) {  // ne changez pas cette ligne
            System.out.println("bravo");
        }
    }

    /*
     * Changez les valeurs de valeur1 et valeur2 afin que "bravo" soit affiché
     *
     * Note: il est possible que les valeurs originales soient bonnes!
     */
    public void bool2() {
        boolean valeur1, valeur2;

        valeur1 = false;
        valeur2 = true;

        if (valeur1 || valeur2) {  // ne changez pas cette ligne
            System.out.println("bravo");
        }
    }

    /*
     * Changez les valeurs de valeur1 et valeur2 afin que "bravo" soit affiché
     *
     * Note: il est possible que les valeurs originales soient bonnes!
     */
    public void bool3() {
        boolean valeur1, valeur2;

        valeur1 = false;
        valeur2 = true;

        if (valeur1 ^ valeur2) {  // ne changez pas cette ligne
            System.out.println("bravo");
        }
    }

    /*
     * Changez les valeurs de valeur1 et valeur2 afin que "bravo" soit affiché
     *
     * Note: il est possible que les valeurs originales soient bonnes!
     */
    public void bool4() {
        boolean valeur1, valeur2;

        valeur1 = false;
        valeur2 = false;

        if (valeur1 || !valeur2) {  // ne changez pas cette ligne
            System.out.println("bravo");
        }
    }

    /*
     * Changez les valeurs de valeur1 et valeur2 et valeur3 afin que "bravo" soit affiché
     *
     * Note: il est possible que les valeurs originales soient bonnes!
     */
    public void bool5() {
        boolean valeur1, valeur2, valeur3;

        valeur1 = false;
        valeur2 = false;
        valeur3 = true;

        if (!(valeur1 && valeur2) && valeur3) {  // ne changez pas cette ligne
            System.out.println("bravo");
        }
    }


    /*
     * Changez les valeurs de valeur1 et valeur2 et valeur3 afin que "bravo" soit affiché
     *
     * Note: il est possible que les valeurs originales soient bonnes!
     */
    public void bool6() {
        boolean valeur1, valeur2, valeur3;

        valeur1 = false;
        valeur2 = false;
        valeur3 = true;

        if ((valeur1 && valeur2) || valeur3) {  // ne changez pas cette ligne
            System.out.println("bravo");
        }
    }

    /*
     * Changez les valeurs de valeur1, valeur2, valeur3 et valeur4 afin que "bravo" soit affiché
     *
     * Note: il est possible que les valeurs originales soient bonnes!
     */
    public void bool7() {
        boolean valeur1, valeur2, valeur3, valeur4;

        valeur1 = false;
        valeur2 = false;
        valeur3 = true;
        valeur4 = true;

        if ((valeur1 && valeur2) || (valeur3 && valeur4)) {  // ne changez pas cette ligne
            System.out.println("bravo");
        }
    }

    /*
     * Modifier le code afin de demander à l'usager un entier.
     * Vous devez ensuite valider que cet entier est entre 0 et 42 (inclusivement).
     *
     * IMPORTANT: Vous devez utiliser un || (OU) dans votre test.
     *
     * Si c'est entre 0 et 42, affichez "OK"
     * Sinon, affichez "erreur"
     *
     * Note: les tests automatisés ne fourniront que des entiers.
     */
    public void boolEtClavier1() {
        Scanner clavier = new Scanner(System.in);
        int i;
        System.out.println("valeur: ");

        i = clavier.nextInt();
        if ( (i <42 )||( i >0 )) {
            System.out.println("OK");
        } else {
            System.out.println("erreur");
        }

    }


    /*
     * Comme au numéro précédent, modifier le code afin de demander à l'usager un entier.
     * Vous devez ensuite valider que cet entier est entre 0 et 42 (inclusivement).
     *
     * IMPORTANT: Cette fois-ci, vous devez utiliser un && (ET) dans votre test.
     *
     * Si c'est entre 0 et 42, affichez "OK"
     * Sinon, affichez "erreur"
     *
     * Note: les tests automatisés ne fourniront que des entiers.
     */
    public void boolEtClavier2() {
        Scanner clavier = new Scanner(System.in);
        int i;
        System.out.println("valeur: ");

        i = clavier.nextInt();
        if ( i <42 && i >0) {
            System.out.println("OK");
        } else {
            System.out.println("erreur");
        }

    }

    /*
     * Modifiez ce code afin de vérifier si le nombre entré est un multiple de 3
     *
     * Si c'est un multiple de 3, affichez "OK"
     * Sinon, affichez "non"
     *
     * indice: opérateur modulo
     */
    public void boolEtClavier3() {
        Scanner clavier = new Scanner(System.in);
        int i;
        System.out.println("valeur: ");
        i = clavier.nextInt();

        if (i % 3 == 0) {
            System.out.println("OK");
        } else {
            System.out.println("non");
        }

    }

    /*
     * Modifiez ce code afin de vérifier si le nombre entré est un multiple de 3 ou de 6
     *
     * Si c'est un multiple de 6, affichez "6"
     * Si c'est un multiple de 3 mais pas de 6, affichez "3"  (ex: 9 est un multiple de 3 mais pas de 6)
     * Si ce n'est ni un multiple de 3 ni de 6, affichez "non"
     *
     * indice: opérateur modulo, et conditions imbriquées.
     */
    public void boolEtClavier4() {
        Scanner clavier = new Scanner(System.in);
        int i;
        System.out.println("valeur: ");
        i = clavier.nextInt();

        if (i % 3 == 0) {
            if (i % 6 ==0) {
                System.out.println("6");
            } else {
                System.out.println("3");
            }
        } else {
            System.out.println("non");
        }

    }

    /*
     * Modifiez ce code afin de vérifier si le nombre entré est un multiple de 3 ET de 4
     *
     * Si c'est un multiple de 3 et de 4, affichez "OK"
     * Sinon, affichez "non"
     *
     * Exemple: 24 est un multiple de 3 et de 4 (3*8 et 4*6),
     * mais 30 n'est pas un multiple de 3 et de 4 (3*10, mais 4 * ??? )
     *
     * indice: opérateur modulo, et conditions booléennes.
     */
    public void boolEtClavier5() {
        Scanner clavier = new Scanner(System.in);
        int i;
        System.out.println("valeur: ");
        i = clavier.nextInt();

        if ( (i %3 == 0) && (i %4 ==0)) {
            System.out.println("OK");
        } else {
            System.out.println("non");

        }
    }
}
